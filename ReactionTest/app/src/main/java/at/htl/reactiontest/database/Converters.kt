package at.htl.reactiontest.database

import androidx.room.TypeConverter
import java.time.LocalDate

class Converters {

    // Converter to save a date in database
    @TypeConverter
    fun fromDate(date: LocalDate): String {
        return date.toString()
    }

    @TypeConverter
    fun toDate(date: String): LocalDate {
        return LocalDate.parse(date)
    }

}