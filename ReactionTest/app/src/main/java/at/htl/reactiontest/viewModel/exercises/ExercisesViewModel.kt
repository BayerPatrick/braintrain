package at.htl.reactiontest.viewModel.exercises

import androidx.navigation.NavController
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.util.*

// definition of global function for exercises

fun getBrainTrainName(navController: NavController): String? {
    return navController.currentBackStackEntry?.destination?.route
}

fun toMilliseconds(string: String): Long {
    //val format = DateTimeFormatter.ofPattern("mm:ss:[SSS]")
    //return LocalDateTime.parse(string, format).atZone(ZoneId.systemDefault()).toInstant().toEpochMilli()    //val formatter = SimpleDateFormat("mm:ss:SSS")
    //val formatter = SimpleDateFormat("mm:ss:SSS")
    //return formatter.parse(string)
    return ofMinSecondMillisSecond(string)
}

fun ofMinSecondMillisSecond(time: String): Long {
    val split = time.split(":")
    val min = split[0].toInt()
    val s = split[1].toInt()
    val ms = split[2].toInt()
    val date = LocalTime.of(0,min,s,ms*1_000_000)
    return date.toNanoOfDay()/1000000
}

fun getLastMonth(): Date {
    var calendar = Calendar.getInstance()
    if(calendar.get(Calendar.MONTH) == Calendar.JANUARY){
        calendar.set(Calendar.YEAR, calendar.get(Calendar.YEAR)-1);
        calendar.set(Calendar.MONTH, Calendar.DECEMBER);
    }else{
        calendar.set(Calendar.MONTH, calendar.get(Calendar.MONTH)-1);
    }
    return calendar.time
}

