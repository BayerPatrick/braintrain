package at.htl.reactiontest.view.exercises

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import at.htl.reactiontest.ui.theme.black
import at.htl.reactiontest.ui.theme.dark_red

// view of header
@Composable
fun HeaderTimer(
    mills: String,
    penaltyTime: Int,
    endResult: Int,
    endPenaltyTime: Int
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .height(60.dp)
            .background(MaterialTheme.colors.primary),
        verticalArrangement = Arrangement.Center
    ) {
        val textResult = when {
            endResult != 0 -> "$endResult ms"
            mills == "" -> {
                "00:00:000"
            }
            else -> {
                mills
            }
        }
        val textPenalty =
            if (endPenaltyTime != 0) "+ $endPenaltyTime ms" else if (penaltyTime != 0) "+ $penaltyTime ms" else ""

        Row(
            modifier = Modifier.fillMaxSize(),
            verticalAlignment = Alignment.CenterVertically
        ) {
            Text(
                text = textPenalty,
                fontSize = 28.sp,
                color = dark_red,
                modifier = Modifier
                    .fillMaxWidth(0.5f)
                    .padding(start = 10.dp),
                textAlign = TextAlign.Start
            )

            Text(
                text = textResult,
                fontSize = 35.sp,
                modifier = Modifier
                    .fillMaxWidth()
                    .padding(end = 10.dp),
                textAlign = TextAlign.End
            )
        }
    }
    Divider(color = black, thickness = 2.dp)
}
