package at.htl.reactiontest.view.exercises

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import at.htl.reactiontest.R
import at.htl.reactiontest.model.BrainTrain
import at.htl.reactiontest.ui.theme.dark_green
import at.htl.reactiontest.ui.theme.dark_red
import at.htl.reactiontest.ui.theme.light_green
import at.htl.reactiontest.ui.theme.light_red
import at.htl.reactiontest.viewModel.exercises.BrainTrainViewModel
import org.koin.androidx.compose.viewModel

// screen of the brainTrainExercise 02
@ExperimentalFoundationApi
@Composable
fun BrainTrain02Screen(navController: NavController, brainTrain: BrainTrain) {
    val brainTrainViewModel: BrainTrainViewModel by viewModel()

    // LiveData (Data-Binding) to all needed values out of the brainTrainViewModel
    val started = brainTrainViewModel.started.observeAsState(initial = false).value
    val finished = brainTrainViewModel.finished.observeAsState(initial = false).value
    val mills = brainTrainViewModel.stopWatchValue.collectAsState().value
    val endResult = brainTrainViewModel.endResult.observeAsState(initial = 0).value
    val penaltyTime = brainTrainViewModel.penaltyTime.observeAsState(initial = 0).value
    val endPenaltyTime = brainTrainViewModel.endPenaltyTime.observeAsState(initial = 0).value

    val visible = brainTrainViewModel.visible.observeAsState(initial = false).value
    val mistake = brainTrainViewModel.mistake.observeAsState(initial = false).value
    val temporarilyValue = brainTrainViewModel.temporarilyValue.observeAsState(initial = 0L).value

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        HeaderTimer(
            mills = mills,
            penaltyTime = penaltyTime,
            endResult = endResult,
            endPenaltyTime = endPenaltyTime
        )

        if (started) {
            // show main content when exercise is started
            MainContentBT02(
                brainTrainViewModel = brainTrainViewModel,
                brainTrain = brainTrain,
                navController = navController,
                visible = visible,
                mistake = mistake,
                temporarilyValue = temporarilyValue
            )
        } else {
            // show logic content when exercise is not started
            LogicContent(
                brainTrain = brainTrain,
                brainTrainViewModel = brainTrainViewModel,
                navController = navController,
                finished = finished,
                endResult = endResult,
                endPenaltyTime = endPenaltyTime
            )
        }

        Row(modifier = Modifier.wrapContentSize(align = Alignment.BottomStart)) {
            FooterResult(brainTrainName = brainTrain.link)
        }
    }
}

// view for main content of exercise 01
@ExperimentalFoundationApi
@Composable
fun MainContentBT02(
    brainTrainViewModel: BrainTrainViewModel,
    brainTrain: BrainTrain,
    navController: NavController,
    visible: Boolean,
    mistake: Boolean,
    temporarilyValue: Long
) {
    BoxWithConstraints {
        val height = maxHeight - 180.dp
        Row(
            Modifier
                .height(height = height)
                .fillMaxWidth()
                .background(MaterialTheme.colors.primaryVariant),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            if (!mistake) {
                if (!visible) {
                    Circle(
                        colorCircle = dark_red,
                        colorText = light_red,
                        text = "WAIT!",
                        brainTrainViewModel = brainTrainViewModel,
                        brainTrainName = brainTrain.link,
                        temporarilyValue = temporarilyValue
                    )
                } else {
                    Circle(
                        colorCircle = dark_green,
                        colorText = light_green,
                        text = "GO!",
                        brainTrainViewModel = brainTrainViewModel,
                        brainTrainName = brainTrain.link,
                        temporarilyValue = temporarilyValue
                    )
                }
            } else {
                Column(
                    verticalArrangement = Arrangement.Center,
                    horizontalAlignment = Alignment.CenterHorizontally
                ) {
                    Text(
                        "Too Early!",
                        fontSize = 40.sp,
                        fontWeight = FontWeight.Bold,
                        color = dark_red,
                        modifier = Modifier.padding(8.dp)
                    )
                    Text(
                        "You need to restart your exercise!!",
                        fontSize = 30.sp,
                        color = dark_red,
                        modifier = Modifier.padding(bottom = 10.dp),
                        textAlign = TextAlign.Center
                    )
                    ReturnButtons(
                        navController = navController,
                        exerciseLink = brainTrain.link
                    )
                }
            }
        }
    }
}

@Composable
fun Circle(
    colorCircle: Color,
    colorText: Color,
    text: String,
    brainTrainViewModel: BrainTrainViewModel,
    brainTrainName: String,
    temporarilyValue: Long
) {
    // show right circle
    Column(horizontalAlignment = Alignment.CenterHorizontally) {
        Box(
            modifier = Modifier
                .size(250.dp)
                .clip(CircleShape)
                .background(colorCircle)
                .clickable {
                    // check if right circle got clicked
                    brainTrainViewModel.checkButtonClickBT02(
                        colorCircle,
                        brainTrainName = brainTrainName
                    )
                },
            contentAlignment = Alignment.Center
        ) {
            Text(text = text, fontSize = 55.sp, fontWeight = FontWeight.Bold, color = colorText)
        }
        // show value for each try
        if (temporarilyValue != 0L) {
            Text(
                text = "$temporarilyValue ms",
                fontSize = 35.sp,
                modifier = Modifier.padding(top = 5.dp),
                textAlign = TextAlign.Center
            )
        } else {
            Text(
                text = "000 ms",
                fontSize = 35.sp,
                modifier = Modifier.padding(top = 5.dp),
                textAlign = TextAlign.Center
            )
        }
    }
}