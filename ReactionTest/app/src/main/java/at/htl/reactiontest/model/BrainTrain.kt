package at.htl.reactiontest.model

// brainTrainExercise model
data class BrainTrain (

    val link: String,
    val brainTrainName: String,
    val description: String,
    val brainTrainType: EBrainTrainType

)
