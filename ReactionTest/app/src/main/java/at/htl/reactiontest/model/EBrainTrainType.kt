package at.htl.reactiontest.model

// enum for type of brainTrain
enum class EBrainTrainType {
    Reaction,
    Speed,
    BrainWork,
    All
}