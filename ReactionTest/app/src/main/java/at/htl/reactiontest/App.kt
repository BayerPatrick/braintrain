package at.htl.reactiontest

import android.app.Application
import androidx.room.Room
import at.htl.reactiontest.database.AppDatabase
import at.htl.reactiontest.viewModel.MainViewModel
import at.htl.reactiontest.viewModel.SettingsViewModel
import at.htl.reactiontest.viewModel.exercises.*
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App : Application() {

    // start app
    override fun onCreate() {
        super.onCreate()

        // start dependency injector
        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(appModule)
        }
    }

}


val appModule = module {
    // set all viewModels
    // singleton
    single { MainViewModel() }
    viewModel { BrainTrainViewModel(get()) }
    viewModel { ResultsViewModel(get()) }
    viewModel { SettingsViewModel(get()) }
    // build room-database
    single {
        Room.databaseBuilder(
            androidContext(),
            AppDatabase::class.java,
            "brain-train-db"
        ).build()
    }
}