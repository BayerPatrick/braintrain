package at.htl.reactiontest.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import androidx.room.TypeConverters
import at.htl.reactiontest.database.Converters
import java.time.LocalDate

// result model
@Entity(tableName = "results")
@TypeConverters(Converters::class)
data class Result (

    @PrimaryKey(autoGenerate = true) val id: Long = 0,

    @ColumnInfo(name = "brainTrainName") val brainTrainName: String,

    @ColumnInfo(name = "created_at") var createdAt: LocalDate,

    @ColumnInfo(name = "result_mills") var resultMills: Long

    )
