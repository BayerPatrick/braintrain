package at.htl.reactiontest.database

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import at.htl.reactiontest.model.Result

@Dao
interface ResultDAO {

    // Database access

    // add Result to database
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addResult(result: Result)

    // get all Results from database as LiveData
    @Query("SELECT * FROM results ORDER BY result_mills")
    fun getAllResultsLiveData(): LiveData<List<Result>>

    // get all Results from database as List
    @Query("SELECT * FROM results ORDER BY result_mills")
    fun getAllResults(): List<Result>

    // get only top ten results from database for specific exercise
    @Query("SELECT * FROM RESULTS WHERE brainTrainName = :brainTrainName ORDER BY result_mills LIMIT 9")
    fun getTopTenMills(brainTrainName: String): LiveData<List<Result>>

}