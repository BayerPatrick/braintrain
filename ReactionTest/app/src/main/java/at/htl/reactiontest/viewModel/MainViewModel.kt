package at.htl.reactiontest.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import at.htl.reactiontest.model.BrainTrain
import at.htl.reactiontest.model.EBrainTrainType

// mainViewModel
class MainViewModel : ViewModel() {

    private val _listOfExercises = MutableLiveData(exercisesList)

    val listOfExercises: LiveData<List<BrainTrain>> = _listOfExercises

    // filter exercises per type
    fun filter(brainTrainType: EBrainTrainType) {
        if (brainTrainType == EBrainTrainType.All)
            _listOfExercises.postValue(exercisesList)
        else {
            val newList = exercisesList.filter { item ->
                item.brainTrainType == brainTrainType
            }
            _listOfExercises.postValue(newList)
        }
    }

}

// definition of all exercises
// static (so no database)
val exercisesList: List<BrainTrain> = listOf(
    BrainTrain(
        link = "brainTrain01",
        brainTrainName = "Color Press Game!",
        description = "Versuche so schnell wie möglich, auf das Rechteck mit der richtigen Farbe zu drücken!;;" +
                "Wird der Start-Button gedrückt, beginnt die Zeit schon zum Laufen. " +
                "Als erstes erscheint nur eine Farbe, welche die gesuchte Farbe für alle Durchgänge sein wird. " +
                "Wird auf diese Farbe gedrückt, erscheinen ein gewisse Anzahl an anderen Vierecken mit verschiedenen Farben. " +
                "Das Ziel ist es nun so schnell wie möglich auf die gesuchte Farbe zu drücken. " +
                "Dies erfolgt jetzt ofter hintereinander, bis alle Durchgänge abgeschlossen sind. Zwischen den Durchgängen sind keine Pausen. " +
                "Wird auf das Rechteck mit der falschen Farbe gedrückt, wird dem Endresultat eine Strafsekunde hinzugefügt!",
        brainTrainType = EBrainTrainType.Speed
    ),
    BrainTrain(
        link = "brainTrain02",
        brainTrainName = "Test your Reaction!",
        description = "Deine pure Reaktionszeit wird getestet!;;" +
                "Wird der Start-Button gedrückt, erscheint ein roter Kreis mitten am Bildschirm. " +
                "Ändert dieser seine Farbe auf Grün, muss so schnell wie möglich der Kreis gedrückt werden. " +
                "Deine Reaktionszeit wird gespeichert und der Kreis ändert seine Farbe wieder auf Rot und das Prozedere beginnt von neu. " +
                "Dieser erfolgt insgesamt sechsmal und am Ende wird die Durchschnittsreaktionzeit aller Durchgänge als Ergebnis genommen. " +
                "Bei einem Fehler ist der Versuch ungültig und die Übung wird abgebrochen!",
        brainTrainType = EBrainTrainType.Reaction
    ),
    BrainTrain(
        link = "brainTrain03",
        brainTrainName = "Most of the Color!",
        description = "Finde die Farbe, die am Häufigsten vorkommt!;;" +
                "Wird der Start-Button gedrückt, erscheint eine gewisse Anzahl an Vierecken am Bildschirm. " +
                "Versuch nun so schnell wie möglich eines von den Vierecken, dessen Farbe am Häufigsten am Bildschirm ist, anzudrücken. " +
                "Es ist auch möglich, dass eine Gleiche Anzahl an Viereck von zwei oder mehr verschiedenen Farben vorhanden ist. " +
                "Ist dies der Fall, gelten alle als Richtig. " +
                "Dieses Prozedere erfolgt nun öfters hintereinander. " +
                "Wird einmal das falsche Viereck gedrückt, wird der Enzeit eine Sekunde hinzugefügt!",
        brainTrainType = EBrainTrainType.BrainWork
    ),
    BrainTrain(
        link = "brainTrain04",
        brainTrainName = "Read the Color!",
        description = "It's about Work",
        brainTrainType = EBrainTrainType.All
    ),
    BrainTrain(
        link = "brainTrain05",
        brainTrainName = "I am the best Two",
        description = "It's about Work",
        brainTrainType = EBrainTrainType.All
    ),
    BrainTrain(
        link = "brainTrain06",
        brainTrainName = "I am difficult",
        description = "It's about Work",
        brainTrainType = EBrainTrainType.All
    )
)