package at.htl.reactiontest.ui.theme

import androidx.compose.foundation.isSystemInDarkTheme
import androidx.compose.material.MaterialTheme
import androidx.compose.material.darkColors
import androidx.compose.material.lightColors
import androidx.compose.runtime.Composable

// definition of all themes

private val DarkColorPalette = darkColors(
    primary = dm_dark_blue,
    primaryVariant = dm_light_blue,
    secondary = dm_selected_light_blue,
    onSecondary = dm_selected_dark_blue
)

private val LightColorPalette = lightColors(
    primary = lm_dark_blue,
    primaryVariant = lm_light_blue,
    secondary = lm_selected_light_blue,
    onSecondary = lm_selected_dark_blue

    /* Other default colors to override
    background = Color.White,
    surface = Color.White,
    onPrimary = Color.White,
    onBackground = Color.Black,
    onSurface = Color.Black,
    error = Color.Black
    */
)

// set own MaterialTheme fpr whole app
@Composable
fun ReactionTestTheme(
    darkTheme: Boolean = isSystemInDarkTheme(),
    content: @Composable() () -> Unit
) {
    /*val systemUiController = rememberSystemUiController()
        systemUiController.setSystemBarsColor(
            color = lm_dark_blue
        )*/

    val colors = if (darkTheme) {
        DarkColorPalette
    } else {
        LightColorPalette
    }

    // MaterialTheme contains all defined colors, shapes, types and themes
    // makes them usable anywhere in the app
    MaterialTheme(
        colors = colors,
        typography = Typography,
        shapes = Shapes,
        content = content
    )
}