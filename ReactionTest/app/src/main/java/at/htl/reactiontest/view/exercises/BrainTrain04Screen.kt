package at.htl.reactiontest.view.exercises

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import at.htl.reactiontest.model.BrainTrain
import at.htl.reactiontest.viewModel.exercises.BrainTrainViewModel
import org.koin.androidx.compose.viewModel

// screen of the brainTrainExercise 04
@Composable
fun BrainTrain04Screen(navController: NavController, brainTrain: BrainTrain) {
    val brainTrainViewModel: BrainTrainViewModel by viewModel()

    // LiveData (Data-Binding) to all needed values out of the brainTrainViewModel
    val started = brainTrainViewModel.started.observeAsState(initial = false).value
    val finished = brainTrainViewModel.finished.observeAsState(initial = false).value
    val mills = brainTrainViewModel.stopWatchValue.collectAsState().value
    val endResult = brainTrainViewModel.endResult.observeAsState(initial = 0).value
    val penaltyTime = brainTrainViewModel.penaltyTime.observeAsState(initial = 0).value
    val endPenaltyTime = brainTrainViewModel.endPenaltyTime.observeAsState(initial = 0).value

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {

        HeaderTimer(
            mills = mills,
            penaltyTime = penaltyTime,
            endResult = endResult,
            endPenaltyTime = endPenaltyTime
        )

        if (started) {
            // show main content when exercise is started
            MainContentBT04(
                brainTrainViewModel = brainTrainViewModel,
                brainTrain = brainTrain,
                mills = mills,
                penaltyTime = penaltyTime,
                started = started,
                navController = navController
            )
        } else {
            // show logic content when exercise is not started
            LogicContent(
                brainTrain = brainTrain,
                brainTrainViewModel = brainTrainViewModel,
                navController = navController,
                finished = finished,
                endResult = endResult,
                endPenaltyTime = endPenaltyTime
            )
        }

        Row(modifier = Modifier.wrapContentSize(align = Alignment.BottomStart)) {
            FooterResult(brainTrainName = brainTrain.link)
        }
    }
}

@Composable
fun MainContentBT04(
    brainTrainViewModel: BrainTrainViewModel,
    brainTrain: BrainTrain,
    navController: NavController,
    mills: String,
    penaltyTime: Int,
    started: Boolean
) {
    BoxWithConstraints {
        val height = maxHeight - 180.dp
        Row(
            Modifier
                .height(height = height)
                .fillMaxWidth()
                .background(MaterialTheme.colors.primaryVariant),
            verticalAlignment = Alignment.CenterVertically,
            horizontalArrangement = Arrangement.Center
        ) {
            Text(text = brainTrain.brainTrainName)
        }
    }
}