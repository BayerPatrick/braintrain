package at.htl.reactiontest.viewModel.exercises

import androidx.compose.ui.graphics.Color
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import at.htl.reactiontest.core.*
import at.htl.reactiontest.database.AppDatabase
import at.htl.reactiontest.model.ColorHolder
import at.htl.reactiontest.model.Result
import at.htl.reactiontest.ui.theme.*
import kotlinx.coroutines.*
import java.time.LocalDate
import kotlin.math.roundToInt
import kotlin.math.sqrt
import kotlin.random.Random

class BrainTrainViewModel(
    db: AppDatabase
) : ViewModel() {

    // viewModel for database
    //region Database
    private val brainTrainDAO = db.brainTrainDAO()

    private val _avg = MutableLiveData(0)
    val avg: LiveData<Int> = _avg

    private val _highScore = MutableLiveData(0)
    val highScore: LiveData<Int> = _highScore

    // generate average and highScore of exercise
    fun generateResults(brainTrainName: String) {
        viewModelScope.launch(Dispatchers.IO) {
            var sum = 0L
            var counter = 0
            var highScore: Long = 0

            val originalList = brainTrainDAO.getAllResults()

            val list = originalList.filter { item ->
                item.brainTrainName == brainTrainName
            }

            if (list.isEmpty()) {
                return@launch
            }

            for (l in list) {
                if (l.resultMills <= highScore) {
                    highScore = l.resultMills
                } else if (highScore == 0L) {
                    highScore = l.resultMills
                }
                sum += l.resultMills
                counter += 1
            }

            val calc = sum.toFloat() / list.size.toFloat()

            _avg.postValue(calc.roundToInt())
            _highScore.postValue(highScore.toInt())
        }
    }

    // add result to database
    fun addResult(result: Result) {
        viewModelScope.launch(Dispatchers.IO) {
            brainTrainDAO.addResult(result = result)
        }
    }
//endregion

    // viewModel for stopWatch
    //region Stopwatch

    //define stopWatch values
    private val provider = object : TimestampProvider {
        override fun getMilliseconds(): Long {
            return System.currentTimeMillis()
        }
    }
    private val elapsedTimeCalculator = ElapsedTimeCalculator(provider)
    private val stopwatch = StopwatchListOrchestrator(
        stopwatchStateHolder = StopwatchStateHolder(
            stopwatchStateCalculator = StopwatchStateCalculator(
                timestampProvider = provider,
                elapsedTimeCalculator = elapsedTimeCalculator
            ),
            elapsedTimeCalculator = elapsedTimeCalculator,
            timestampMillisecondsFormatter = TimestampMillisecondsFormatter()
        ),
        scope = viewModelScope
    )
    val stopWatchValue = stopwatch.ticker

    // define logic values
    private val _started = MutableLiveData(false)
    val started: LiveData<Boolean> = _started

    private val _finished = MutableLiveData(false)
    val finished: LiveData<Boolean> = _finished

    private val _endResult = MutableLiveData(0)
    val endResult: LiveData<Int> = _endResult

    private val _endPenaltyTime = MutableLiveData(0)
    val endPenaltyTime: LiveData<Int> = _endPenaltyTime

    // start stopWatch and set values
    fun start() {
        stopwatch.start()
        _finished.value = false
        _started.value = true
        _endResult.value = 0
        _endPenaltyTime.value = 0
    }

    // stop stopWatch and set values
    private fun stop(brainTrainName: String, penTime: Int) {
        val res = stopWatchValue.value
        stopwatch.stop()
        _endResult.value = toMilliseconds(res).toInt()
        _endPenaltyTime.value = penTime
        _started.value = false
        _finished.value = true
        addResult(
            Result(
                brainTrainName = brainTrainName,
                createdAt = LocalDate.now(),
                resultMills = toMilliseconds(res) + penTime
            )
        )
    }
//endregion

    // viewModel for brainTrain01
    //region BrainTrain01

    // define all values for brainTrain01
    private val _colorList = MutableLiveData<List<ColorHolder>>()
    val colorsList: LiveData<List<ColorHolder>> = _colorList

    private val win = MutableLiveData(false)

    private val _penaltyTime = MutableLiveData(0)
    val penaltyTime: LiveData<Int> = _penaltyTime

    private val _winColor = MutableLiveData<ColorHolder>()
    val winColor: LiveData<ColorHolder> = _winColor

    private var repetitions = 0

    // list of all available colors
    private val allColorsList = listOf(
        ColorHolder(name = "dark_green", color = dark_green),
        ColorHolder(name = "dark_blue", color = dark_blue),
        ColorHolder(name = "dark_red", color = dark_red),
        ColorHolder(name = "dark_yellow", color = dark_yellow),
        ColorHolder(name = "dark_brown", color = dark_brown),
        ColorHolder(name = "dark_black", color = dark_black),
        ColorHolder(name = "dark_purple", color = dark_purple),
        ColorHolder(name = "dark_orange", color = dark_orange),
        ColorHolder(name = "dark_cyan", color = dark_cyan),
    )

    // generate colors/ start exercise one
    fun generateColorsBT01(amount: Int) {
        val colorsList = allColorsList.subList(0, amount).shuffled()

        // set win on first call
        if (winColor.value == null) {
            val random = Random.nextInt(from = 0, until = colorsList.size - 1)
            _winColor.value = colorsList[random]
            val list = ArrayList<ColorHolder>()
            list.add(colorsList[random])
            _colorList.value = list
        } else {
            if (repetitions < 3) {
                _colorList.value = colorsList
            } else {
                this._colorList.value = allColorsList.subList(0, 9).shuffled()
            }
            repetitions++
        }
    }

    // check clicked field
    fun checkButtonClickBT01(color: Color, brainTrainName: String, penTime: Int) {
        if (color == _winColor.value?.color) {
            if (repetitions >= 6) {
                win.value = true
                stop(brainTrainName, penTime)
                clearValuesBT01()
            }
            generateColorsBT01(4)
        } else {
            _penaltyTime.value = _penaltyTime.value?.plus(1000)
        }
    }

    // clear all used values
    private fun clearValuesBT01() {
        repetitions = 0
        _penaltyTime.value = 0
    }

    // get sqrt (for view)
    fun getSqrt(amount: Int): Int {
        return sqrt(amount.toFloat()).toInt()
    }
//endregion

    //viewModel for brainTrain02
    //region BrainTrain02

    // define all values for brainTrain02
    private val _visible = MutableLiveData(false)
    val visible: LiveData<Boolean> = _visible

    private val _mistake = MutableLiveData(false)
    val mistake: LiveData<Boolean> = _mistake

    private val _temporarilyValue = MutableLiveData(0L)
    val temporarilyValue: LiveData<Long> = _temporarilyValue

    private var _reactionMillsSum = 0L
    private var _random = 0

    // start (first time)
    fun firstStartReaction() {
        _started.value = true
        _mistake.value = false
    }

    // start round of exercise
    fun startReaction() {
        viewModelScope.launch {
            _random = (2000..6000).random()

            delay(_random.toLong())
            _visible.postValue(true)
            start()
        }
    }

    // check if right circle got clicked (start new round when repetitions isn't enough)
    fun checkButtonClickBT02(color: Color, brainTrainName: String) {

        if (color == dark_green) {
            if (repetitions >= 5) {
                stopwatch.stop()
                _started.value = false
                _finished.value = true
                _endResult.value = (_reactionMillsSum / repetitions).toFloat().roundToInt()
                _endPenaltyTime.value = 0
                addResult(
                    Result(
                        brainTrainName = brainTrainName,
                        createdAt = LocalDate.now(),
                        resultMills = _reactionMillsSum / repetitions
                    )
                )
                clearValuesBT2()
            } else {
                val res = stopWatchValue.value
                stopwatch.stop()
                _visible.value = false

                _temporarilyValue.value = toMilliseconds(res)
                _reactionMillsSum += toMilliseconds(res)
                repetitions++

                startReaction()
            }
        } else {
            _mistake.value = true
            stopwatch.stop()
        }
    }

    // clear all values
    private fun clearValuesBT2() {
        repetitions = 0
        _reactionMillsSum = 0
        _temporarilyValue.value = 0
    }
//endregion

    // vieModel brainTrain03
    //region BrainTrain03

    // define values
    private var _winColorList = ArrayList<Color>()

    // generate field (all colors) = start exercise
    fun generateColorsBT03() {
        _winColorList = ArrayList()
        val list = ArrayList<ColorHolder>()
        if (repetitions < 3) {
            for (i in 0..8) {
                val random = Random.nextInt(from = 0, until = 8)
                list.add(allColorsList[random])
            }
        } else {
            for (i in 0..26) {
                val random = Random.nextInt(from = 0, until = 8)
                list.add(allColorsList[random])
            }
        }

        val numbersByElement = list.groupingBy { it }.eachCount()
        val maxValue = numbersByElement.maxByOrNull { it.value }?.value

        for (col in numbersByElement) {
            if (col.value == maxValue) {
                _winColorList.add(col.key.color)
            }
        }

        _colorList.value = list
        repetitions++
    }

    // check if right field got pressed
    fun checkButtonClickBT03(color: Color, brainTrainName: String, penTime: Int) {
        if (_winColorList.contains(color)) {
            if (repetitions >= 6) {
                win.value = true
                stop(brainTrainName, penTime)
                clearValuesBT03()
            }
            generateColorsBT03()
        } else {
            _penaltyTime.value = _penaltyTime.value?.plus(1000)
        }
    }

    // clear all used values
    private fun clearValuesBT03() {
        repetitions = 0
        _penaltyTime.value = 0
        _winColorList = ArrayList()
    }

    //endregion

}