package at.htl.reactiontest.view.exercises

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import at.htl.reactiontest.ui.theme.main_dark_blue
import at.htl.reactiontest.ui.theme.white
import at.htl.reactiontest.viewModel.exercises.BrainTrainViewModel

@Composable
fun Description(
    description: String,
    brainTrainName: String,
    brainTrainViewModel: BrainTrainViewModel
) {
    BoxWithConstraints {
        val height = maxHeight - 180.dp
        val splitDescription: List<String>

        if (description.contains(";;")) {
            splitDescription = description.split(";;")
        } else {
            splitDescription = ArrayList(2)
            splitDescription.add("")
            splitDescription.add("description")
        }


        Column(
            modifier = Modifier
                .fillMaxWidth()
                .height(height)
                .background(MaterialTheme.colors.primaryVariant),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth(0.8f),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = splitDescription[0],
                    fontSize = 16.sp,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center
                )
                Text(
                    text = splitDescription[1],
                    fontSize = 14.sp,
                    textAlign = TextAlign.Center
                )
                Button(
                    modifier = Modifier
                        .padding(top = 10.dp),
                    colors = ButtonDefaults.buttonColors(backgroundColor = main_dark_blue),
                    shape = RoundedCornerShape(4.dp),
                    onClick = {
                        when (brainTrainName) {
                            "brainTrain01" -> {
                                brainTrainViewModel.generateColorsBT01(amount = 4)
                                brainTrainViewModel.start()
                            }
                            "brainTrain02" -> {
                                brainTrainViewModel.firstStartReaction()
                                brainTrainViewModel.startReaction()
                            }
                            "brainTrain03" -> {
                                brainTrainViewModel.generateColorsBT03()
                                brainTrainViewModel.start()
                            }
                            "brainTrain04" ->
                                null
                            "brainTrain05" ->
                                null
                            "brainTrain06" ->
                                null
                        }
                    }
                ) {
                    Text(
                        text = "Start",
                        color = white,
                        modifier = Modifier.background(main_dark_blue)
                    )
                }
            }
        }
    }
}