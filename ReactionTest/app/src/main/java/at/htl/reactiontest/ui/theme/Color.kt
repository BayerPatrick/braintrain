package at.htl.reactiontest.ui.theme

import androidx.compose.ui.graphics.Color

// definition of all colors

val black = Color(0xFF000000)
val gray = Color(0xFF707070)
val light_gray = Color(0xFFBFBFBF)
val white = Color(0xFFFFFFFF)

val main_light_blue = Color(0xFF5670CF)
val main_dark_blue = Color(0xFF12167B)

val dm_dark_blue = Color(0xFF5760BD)
val dm_light_blue = Color(0xFF9CA1D8)
val dm_selected_dark_blue = Color(0xFF3945B1)
val dm_selected_light_blue = Color(0xFF747DC8)

val lm_dark_blue = Color(0xFF747DC8)
val lm_light_blue = Color(0xFFC3C6E8)
val lm_selected_dark_blue = Color(0xFF5760BD)
val lm_selected_light_blue = Color(0xFF9CA1D8)

val light_green = Color(0xFF94FFA2)
val dark_green = Color(0xFF00C11A)
val light_red = Color(0xFFFFA1A1)
val dark_red = Color(0xFFDB0101)
val light_yellow = Color(0xFFF4FC80)
val dark_yellow = Color(0xFFCED848)
val light_blue = Color(0xFFA98FFF)
val dark_blue = Color(0xFF3F16C9)
val light_brown = Color(0xFFC4A394)
val dark_brown = Color(0xFF794C36)
val light_purple = Color(0xFFD390D0)
val dark_purple = Color(0xFF850F95)
val light_black = Color(0xFF8F8F8F)
val dark_black = Color(0xFF313131)
val light_cyan = Color(0xFFA4F6FF)
val dark_cyan = Color(0xFF0BC2C2)
val light_orange = Color(0xFFFED76C)
val dark_orange = Color(0xFFD59B13)

val gold = Color(0xFFFFD700)
val silver = Color(0xFFA9A9A9)
val bronze = Color(0xFFB8860B)