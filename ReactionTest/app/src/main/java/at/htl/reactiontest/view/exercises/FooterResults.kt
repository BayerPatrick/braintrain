package at.htl.reactiontest.view.exercises

import androidx.compose.foundation.background
import androidx.compose.foundation.border
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import at.htl.reactiontest.ui.theme.*
import at.htl.reactiontest.viewModel.exercises.ResultsViewModel
import org.koin.androidx.compose.viewModel

// view of footer
@Composable
fun FooterResult(
    brainTrainName: String
) {
    val resultsViewModel: ResultsViewModel by viewModel()

    val results = resultsViewModel.filteredList.observeAsState(initial = emptyList()).value
    resultsViewModel.filter("All Time", brainTrainName)

    Column(modifier = Modifier.background(MaterialTheme.colors.primary)) {
        ResultHeader(resultsViewModel, brainTrainName)
        Column(
            modifier = Modifier
                .height(150.dp)
                .fillMaxWidth()
        ) {
            Divider(thickness = 1.dp)
            BoxWithConstraints{
                val maxWidth = maxWidth
                val maxHeight = maxHeight
                Row(modifier = Modifier.fillMaxSize()) {
                    val resultListStrings: MutableList<String> = mutableListOf()
                    if (results.isNotEmpty()) {
                        when {
                            results.size >= 9 -> {
                                for (i in results.subList(0, 9)) {
                                    resultListStrings.add(i.resultMills.toString())
                                }
                            }
                            results.size < 9 -> {
                                for (i in results.indices) {
                                    resultListStrings.add(i, results[i].resultMills.toString())
                                }
                                for (i in results.size..8) {
                                    resultListStrings.add(i, "000")
                                }
                            }
                        }
                    } else {
                        for (i in 0..8) {
                            resultListStrings.add(i, "000")
                        }
                    }

                    Result(
                        maxHeight = maxHeight,
                        maxWidth = maxWidth,
                        results = resultListStrings.subList(0, 3),
                        row = 1
                    )
                    Result(
                        maxHeight = maxHeight,
                        maxWidth = maxWidth,
                        results = resultListStrings.subList(3, 6),
                        row = 2
                    )
                    Result(
                        maxHeight = maxHeight,
                        maxWidth = maxWidth,
                        results = resultListStrings.subList(6, 9),
                        row = 3
                    )

                }
            }
        }
    }
}

@Composable
fun ResultHeader(resultsViewModel: ResultsViewModel, brainTrainName: String) {
    Divider(color = black, thickness = 2.dp)
    Row(modifier = Modifier.padding(6.dp)) {
        Text(
            text = "HIGHSCORES:",
            fontWeight = FontWeight.Bold,
            modifier = Modifier.padding(5.dp, 0.dp, 50.dp, 0.dp)
        )
        CustomRadioGroup(resultsViewModel, brainTrainName)
    }
}

@Composable
fun CustomRadioGroup(resultsViewModel: ResultsViewModel, brainTrainName: String) {
    val options = listOf(
        "All Time",
        "This Month"
    )
    var selectedOption by remember {
        mutableStateOf("All Time")
    }
    val onSelectionChange = { text: String ->
        selectedOption = text
        resultsViewModel.filter(brainTrainName = brainTrainName, filter = text)
    }

    Row(modifier = Modifier.border(width = 1.dp, color = black, shape = RoundedCornerShape(2.dp))) {
        options.forEach { text ->
            val selectionBoxShape = if (text == "All Time") {
                RoundedCornerShape(topStart = 2.dp, bottomStart = 2.dp)
            } else {
                RoundedCornerShape(topEnd = 2.dp, bottomEnd = 2.dp)
            }
            Row {
                Text(
                    text = text,
                    fontSize = 15.sp,
                    color = if (text == selectedOption) {
                        white
                    } else {
                        black
                    },
                    modifier = Modifier
                        .clip(shape = selectionBoxShape)
                        .clickable {
                            onSelectionChange(text)
                        }
                        .background(
                            if (text == selectedOption) {
                                MaterialTheme.colors.onSecondary
                            } else {
                                MaterialTheme.colors.secondary
                            }
                        )
                        .padding(vertical = 0.dp, horizontal = 4.dp)
                )
            }
        }
    }
}

@Composable
fun Result(
    maxHeight: Dp,
    maxWidth: Dp,
    results: List<String>,
    row: Int
) {
    val circleHeight = if (row == 1) maxHeight / 7 else maxHeight / 8

    Column(
        modifier = Modifier
            .width(maxWidth / 3)
            .fillMaxHeight()
    ) {
        for (i in 0..2) {
            Row(
                modifier = Modifier
                    .height(maxHeight / 3)
                    .fillMaxWidth()
                    .padding(15.dp, 0.dp, 0.dp, 0.dp)
                    .then(Modifier.wrapContentHeight(align = Alignment.CenterVertically)),
                verticalAlignment = Alignment.CenterVertically
            ) {
                if (row == 1) {
                    val colorListWinners = mutableListOf(gold, silver, bronze)
                    Box(
                        modifier = Modifier
                            .size(circleHeight)
                            .clip(CircleShape)
                            .background(colorListWinners[i])
                    )
                    Text(
                        modifier = Modifier
                            .padding(10.dp, 0.dp, 0.dp, 0.dp),
                        fontSize = 18.sp,
                        textAlign = TextAlign.Center,
                        text = results[i] + " ms"
                    )
                } else {
                    val rankingList =
                        if (row == 2) mutableListOf("4", "5", "6") else mutableListOf("7", "8", "9")
                    Box(
                        modifier = Modifier
                            .size(circleHeight)
                            .clip(CircleShape)
                            .background(light_gray)
                            .border(width = 1.dp, color = gray, shape = CircleShape)
                            .then(
                                Modifier
                                    .wrapContentHeight(align = Alignment.CenterVertically)
                            ),
                        contentAlignment = Alignment.Center
                    ) {
                        Text(text = rankingList[i], fontSize = 10.sp, textAlign = TextAlign.Center)
                    }
                    Text(
                        modifier = Modifier
                            .padding(10.dp, 0.dp, 0.dp, 0.dp),
                        fontSize = 15.sp,
                        textAlign = TextAlign.Center,
                        text = results[i] + " ms"
                    )
                }
            }
            Divider()
        }
    }
    if (row == 1 || row == 2) {
        Column(modifier = Modifier.fillMaxHeight(), verticalArrangement = Arrangement.Center) {
            Divider(
                modifier = Modifier
                    .fillMaxHeight()
                    .width(1.dp)
            )
        }
    }
}