package at.htl.reactiontest.view

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Settings
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import at.htl.reactiontest.ui.theme.ReactionTestTheme
import at.htl.reactiontest.view.exercises.*
import at.htl.reactiontest.viewModel.SettingsViewModel
import at.htl.reactiontest.viewModel.exercisesList
import com.google.accompanist.systemuicontroller.rememberSystemUiController
import org.koin.androidx.compose.viewModel

// mainView
@ExperimentalFoundationApi
@Composable
fun MainView() {

    val systemUiController = rememberSystemUiController()
    val navController = rememberNavController()

    // get value for dark/lightMode
    val settingsViewModel: SettingsViewModel by viewModel()
    val isDark = settingsViewModel.isDark.observeAsState(initial = false).value

    // set own created theme
    ReactionTestTheme(darkTheme = isDark) {
        systemUiController.setSystemBarsColor(
            color = MaterialTheme.colors.primary
        )
        Scaffold(
            // create TopAppBar
            topBar = {
                TopAppBar(
                    backgroundColor = MaterialTheme.colors.primary,
                    elevation = 0.dp,
                    title = {
                        Text(text = "Brain Train")
                    },
                    actions = {
                        Icon(Icons.Default.Settings, contentDescription = "Settings",
                            modifier = Modifier.clickable {
                                //navController.navigate("settings")
                                settingsViewModel.changeDarkMode()
                            })
                    }
                )
            }
        ) {
            // define all routes
            NavHost(navController = navController, startDestination = "home") {
                composable("home") { HomeScreen(navController = navController) }
                composable("brainTrain01") {
                    BrainTrain01Screen(
                        navController = navController,
                        brainTrain = exercisesList[0]
                    )
                }
                composable("brainTrain02") {
                    BrainTrain02Screen(
                        navController = navController,
                        brainTrain = exercisesList[1]
                    )
                }
                composable("brainTrain03") {
                    BrainTrain03Screen(
                        navController = navController,
                        brainTrain = exercisesList[2]
                    )
                }
                composable("brainTrain04") {
                    BrainTrain04Screen(
                        navController = navController,
                        brainTrain = exercisesList[3]
                    )
                }
                composable("brainTrain05") {
                    BrainTrain05Screen(
                        navController = navController,
                        brainTrain = exercisesList[4]
                    )
                }
                composable("brainTrain06") {
                    BrainTrain06Screen(
                        navController = navController,
                        brainTrain = exercisesList[5]
                    )
                }
                /*...*/
            }
        }
    }


}

// function if you want to specify routing
private fun onClickBottomItem(navController: NavController, route: String) {
    navController.navigate(route) {
        /*popUpTo(navController.graph.findStartDestination().id) {
            saveState = true
        }*/
        launchSingleTop = true
        restoreState = true
    }
}