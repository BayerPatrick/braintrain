package at.htl.reactiontest.database

import androidx.room.AutoMigration
import androidx.room.Database
import androidx.room.RoomDatabase
import at.htl.reactiontest.model.Result

// Database Setup with autoMigration
@Database(
    entities = [Result::class],
    version = 4,
    autoMigrations = [
        AutoMigration(from = 1, to = 2),
        AutoMigration(from = 2, to = 3),
        AutoMigration(from = 3, to = 4)
    ]
)
abstract class AppDatabase : RoomDatabase() {

    // used DAO for database access
    abstract fun brainTrainDAO(): ResultDAO

}