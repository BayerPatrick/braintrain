package at.htl.reactiontest.viewModel.exercises

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import at.htl.reactiontest.database.AppDatabase
import at.htl.reactiontest.model.Result
import kotlinx.coroutines.launch
import java.time.LocalDate

// viewModel for results
class ResultsViewModel(
    db: AppDatabase
) : ViewModel() {

    // define values
    private val brainTrainDAO = db.brainTrainDAO()

    private val filter = "All Time"
    private var brainTrainName = ""

    private val _originalList: LiveData<List<Result>> = brainTrainDAO.getAllResultsLiveData()

    private val _filteredList = MutableLiveData<List<Result>>(ArrayList())
    val filteredList: LiveData<List<Result>> = _filteredList

    // get all results on init (async)
    init {
        _originalList.observeForever { list ->
            //DO FILTER HERE
            filter(filter, brainTrainName)
        }

        _filteredList.postValue(_originalList.value)
    }

    // filter all results (on click from all time to this month)
    fun filter(filter: String, brainTrainName: String) {
        viewModelScope.launch {
            val originalList = _originalList.value
            if (filter == "All Time") {
                val list = originalList?.filter { item ->
                    item.brainTrainName == brainTrainName
                }

                _filteredList.postValue(list)
            } else if (filter == "This Month") {
                val list = originalList?.filter { item ->
                    item.brainTrainName == brainTrainName
                            && item.createdAt >= LocalDate.now().minusMonths(1)
                }

                _filteredList.postValue(list)
            }
        }
    }

}