package at.htl.reactiontest.view.exercises

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.material.Card
import androidx.compose.runtime.Composable
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.unit.dp
import androidx.navigation.NavController
import at.htl.reactiontest.model.BrainTrain
import at.htl.reactiontest.model.ColorHolder
import at.htl.reactiontest.viewModel.exercises.BrainTrainViewModel
import org.koin.androidx.compose.viewModel

// screen of the brainTrainExercise 03
@ExperimentalFoundationApi
@Composable
fun BrainTrain03Screen(navController: NavController, brainTrain: BrainTrain) {
    val brainTrainViewModel: BrainTrainViewModel by viewModel()

    // LiveData (Data-Binding) to all needed values out of the brainTrainViewModel
    val started = brainTrainViewModel.started.observeAsState(initial = false).value
    val finished = brainTrainViewModel.finished.observeAsState(initial = false).value
    val mills = brainTrainViewModel.stopWatchValue.collectAsState().value
    val endResult = brainTrainViewModel.endResult.observeAsState(initial = 0).value
    val penaltyTime = brainTrainViewModel.penaltyTime.observeAsState(initial = 0).value
    val endPenaltyTime = brainTrainViewModel.endPenaltyTime.observeAsState(initial = 0).value

    val colors = brainTrainViewModel.colorsList.observeAsState(initial = emptyList()).value

    Column(
        modifier = Modifier.fillMaxSize(),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        HeaderTimer(
            mills = mills,
            penaltyTime = penaltyTime,
            endResult = endResult,
            endPenaltyTime = endPenaltyTime
        )

        if (started) {
            // show main content when exercise is started
            MainContentBT03(
                brainTrainViewModel = brainTrainViewModel,
                brainTrain = brainTrain,
                colors = colors,
                penaltyTime = penaltyTime
            )
        } else {
            // show logic content when exercise is not started
            LogicContent(
                brainTrain = brainTrain,
                brainTrainViewModel = brainTrainViewModel,
                navController = navController,
                finished = finished,
                endResult = endResult,
                endPenaltyTime = endPenaltyTime
            )
        }

        Row(modifier = Modifier.wrapContentSize(align = Alignment.BottomStart)) {
            FooterResult(brainTrainName = brainTrain.link)
        }
    }
}

@ExperimentalFoundationApi
@Composable
fun MainContentBT03(
    brainTrainViewModel: BrainTrainViewModel,
    brainTrain: BrainTrain,
    colors: List<ColorHolder>,
    penaltyTime: Int
) {
    val amountColors = colors.size

    // automatically build view for amount of colors in colors (list)
    BoxWithConstraints {
        val height = maxHeight - 180.dp
        if (colors.isNotEmpty()) {
            LazyVerticalGrid(
                cells = GridCells.Fixed(brainTrainViewModel.getSqrt(amountColors))
            ) {
                items(colors) { item ->
                    Card(
                        shape = RectangleShape,
                        backgroundColor = item.color,
                        modifier = Modifier.clickable {
                            // check if clicked field is right
                            brainTrainViewModel.checkButtonClickBT03(
                                item.color,
                                brainTrain.link,
                                penaltyTime
                            )
                        },
                    ) {
                        // Spacer if colors is empty
                        Spacer(
                            modifier = Modifier.height(
                                height = height / brainTrainViewModel.getSqrt(
                                    amountColors
                                )
                            )
                        )
                    }
                }
            }
        } else {
            // Spacer for loading
            Spacer(modifier = Modifier.height(height))
        }
    }
}