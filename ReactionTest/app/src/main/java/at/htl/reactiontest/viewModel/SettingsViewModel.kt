package at.htl.reactiontest.viewModel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import at.htl.reactiontest.database.AppDatabase

// viewModel for settings
class SettingsViewModel(
    db: AppDatabase
) : ViewModel() {

    private val _isDark = MutableLiveData(false)
    val isDark: LiveData<Boolean> = _isDark

    // function to change light/darkMode
    fun changeDarkMode() {
        _isDark.value = _isDark.value != true
    }

}