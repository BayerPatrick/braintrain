package at.htl.reactiontest.view

import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.foundation.background
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.GridCells
import androidx.compose.foundation.lazy.LazyRow
import androidx.compose.foundation.lazy.LazyVerticalGrid
import androidx.compose.foundation.lazy.items
import androidx.compose.foundation.shape.CircleShape
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Icon
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.*
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.clip
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.text.SpanStyle
import androidx.compose.ui.text.buildAnnotatedString
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.intl.Locale
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.text.toUpperCase
import androidx.compose.ui.text.withStyle
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import at.htl.reactiontest.R
import at.htl.reactiontest.model.BrainTrain
import at.htl.reactiontest.model.EBrainTrainType
import at.htl.reactiontest.ui.theme.main_dark_blue
import at.htl.reactiontest.ui.theme.main_light_blue
import at.htl.reactiontest.ui.theme.white
import at.htl.reactiontest.viewModel.MainViewModel
import org.koin.androidx.compose.get

// view of homeScreen
@ExperimentalFoundationApi
@Composable
fun HomeScreen(
    navController: NavController
) {
    val mainViewModel: MainViewModel = get()

    // get all available exercises and categories
    val listOfExercises =
        mainViewModel.listOfExercises.observeAsState(initial = ArrayList()).value
    val listOfCategories: List<EBrainTrainType> = listOf(
        EBrainTrainType.All,
        EBrainTrainType.Speed,
        EBrainTrainType.Reaction,
        EBrainTrainType.BrainWork
    )

    Box(
        modifier = Modifier
            .background(MaterialTheme.colors.primaryVariant)
            .fillMaxSize()
    ) {
        Column {
            GreetingSection()
            CategoriesSection(categories = listOfCategories)
            BrainTrainCards(brainTrains = listOfExercises, navController = navController)
        }
    }
}

// view for greetingSection
@Composable
fun GreetingSection() {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .padding(15.dp),
        verticalArrangement = Arrangement.Center,
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "Welcome to",
            style = MaterialTheme.typography.h4
        )
        Text(
            style = MaterialTheme.typography.h6,
            text =
            buildAnnotatedString {
                withStyle(
                    style = SpanStyle(
                        color = main_dark_blue,
                        fontSize = 25.sp,
                        letterSpacing = 2.sp,
                        fontWeight = FontWeight.Bold
                    )
                ) {
                    append("(B)")
                }
                withStyle(
                    style = SpanStyle(
                        color = main_light_blue,
                        fontSize = 25.sp,
                        letterSpacing = 10.sp
                    )
                ) {
                    append("rain")
                }
                append(" YOUR")
                withStyle(style = SpanStyle(letterSpacing = 5.sp)) {
                    append(" ")
                }
                withStyle(
                    style = SpanStyle(
                        color = main_light_blue,
                        fontSize = 25.sp,
                        letterSpacing = 2.sp,
                        fontWeight = FontWeight.Bold
                    )
                ) {
                    append("(T)")
                }
                withStyle(
                    style = SpanStyle(
                        color = main_dark_blue,
                        fontSize = 25.sp,
                        letterSpacing = 10.sp
                    )
                ) {
                    append("rain")
                }
            }
        )
        Text(
            text = "Here you can easily improve your Reaction-Time and Brain-Speed while having fun!",
            textAlign = TextAlign.Center,
            fontSize = 16.sp,
            letterSpacing = 2.sp,
            modifier = Modifier.padding(top = 15.dp)
        )

    }
}

// view for all categories
@Composable
fun CategoriesSection(
    categories: List<EBrainTrainType>
) {
    val mainViewModel: MainViewModel = get()
    var selectedCategoriesIndex by remember {
        mutableStateOf(0)
    }
    LazyRow {
        items(categories.size) {
            Box(
                contentAlignment = Alignment.Center,
                modifier = Modifier
                    .padding(start = 15.dp, top = 15.dp, bottom = 15.dp)
                    .clickable {
                        selectedCategoriesIndex = it
                        mainViewModel.filter(categories[it])
                    }
                    .clip(RoundedCornerShape(10.dp))
                    .background(
                        if (selectedCategoriesIndex == it) MaterialTheme.colors.secondary
                        else MaterialTheme.colors.onSecondary
                    )
                    .padding(15.dp)
            ) {
                Text(text = categories[it].toString(), color = white)
            }
        }
    }
}

// view for all brainTrainsCards
@ExperimentalFoundationApi
@Composable
fun BrainTrainCards(brainTrains: List<BrainTrain>, navController: NavController) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(MaterialTheme.colors.secondary),
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Text(
            text = "YOUR EXERCISES",
            style = MaterialTheme.typography.h5,
            modifier = Modifier.padding(top = 10.dp)
        )
        LazyVerticalGrid(
            cells = GridCells.Fixed(1),
            contentPadding = PaddingValues(start = 7.5.dp, end = 7.5.dp, bottom = 60.dp),
            modifier = Modifier.fillMaxHeight()
        ) {
            items(brainTrains) { item ->
                BrainTrainCard(exercise = item, navController = navController)
            }
        }
    }
}

// view for one brainTrainCard
@ExperimentalFoundationApi
@Composable
fun BrainTrainCard(
    exercise: BrainTrain,
    navController: NavController,
    color: Color = main_light_blue
) {
    Row(
        verticalAlignment = Alignment.CenterVertically,
        horizontalArrangement = Arrangement.SpaceBetween,
        modifier = Modifier
            .padding(vertical = 10.dp, horizontal = 15.dp)
            .clip(RoundedCornerShape(10.dp))
            .background(color)
            .padding(15.dp)
            .fillMaxWidth()
    ) {
        Column {
            Text(
                text = exercise.brainTrainName.toUpperCase(Locale.current),
                style = MaterialTheme.typography.h6,
                color = main_dark_blue
            )
            Text(
                text = exercise.brainTrainType.toString(),
                fontSize = 16.sp,
                color = white,
                fontWeight = FontWeight.W300,
                letterSpacing = 5.sp
            )
        }
        Box(
            contentAlignment = Alignment.Center,
            modifier = Modifier
                .size(40.dp)
                .clip(CircleShape)
                .background(main_dark_blue)
                .padding(10.dp)
                .clickable {
                    navController.navigate(exercise.link)
                }
        ) {
            Icon(
                painter = painterResource(id = R.drawable.ic_play),
                contentDescription = "Play",
                tint = white,
                modifier = Modifier.size(16.dp)
            )
        }
    }
}