package at.htl.reactiontest.view.exercises

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.shape.RoundedCornerShape
import androidx.compose.material.Button
import androidx.compose.material.ButtonDefaults
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.testTag
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.text.style.TextAlign
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.navigation.NavController
import at.htl.reactiontest.ui.theme.main_dark_blue
import at.htl.reactiontest.ui.theme.white
import at.htl.reactiontest.viewModel.exercises.BrainTrainViewModel
import kotlin.math.abs

// view for endResult
@Composable
fun EndResult(
    result: String,
    navController: NavController,
    exerciseLink: String,
    viewModel: BrainTrainViewModel
) {

    BoxWithConstraints {
        // get all values and right texts for view (average, highScore...)
        val average = viewModel.avg.observeAsState(initial = 0).value
        val highScore = viewModel.highScore.observeAsState(initial = 0).value

        val height = maxHeight - 180.dp

        val differenceAverage = average - result.toInt()
        val differenceAverageText = abs(differenceAverage)

        val differenceHighScore = result.toInt() - highScore

        val textAverage =
            if (differenceAverage > 0) "$differenceAverageText ms unter" else "$differenceAverageText ms über"
        val textHighScore =
            if (differenceHighScore <= 0) "Damit hast du deinen neuen Highscore aufgestellt!" else "Im Vergleich zu deinem Highscore liegst du $differenceHighScore ms zurück!"

        val textHeader = "Stark! Dein Ergebnis ist $result ms"
        val textBody = "Dein Durchschnitt bei dieser Übung beträgt $average ms, " +
                "somit liegst du $textAverage deinem Durchschnitt! " +
                "$textHighScore Weiter so!"

        Column(
            modifier = Modifier
                .testTag("finish_screen")
                .fillMaxWidth()
                .height(height)
                .background(MaterialTheme.colors.primaryVariant),
            verticalArrangement = Arrangement.Center,
            horizontalAlignment = Alignment.CenterHorizontally
        ) {
            Column(
                modifier = Modifier
                    .fillMaxWidth(0.8f),
                verticalArrangement = Arrangement.Center,
                horizontalAlignment = Alignment.CenterHorizontally
            ) {
                Text(
                    text = textHeader,
                    fontSize = 18.sp,
                    fontWeight = FontWeight.Bold,
                    textAlign = TextAlign.Center
                )
                Text(
                    text = textBody,
                    fontSize = 15.sp,
                    textAlign = TextAlign.Center
                )
                // buttons to get back to exercise- or homesScreen
                ReturnButtons(
                    navController = navController,
                    exerciseLink = exerciseLink
                )
            }
        }
    }
}

// view for buttons to get back to exercise- or homesScreen
@Composable
fun ReturnButtons(
    navController: NavController,
    exerciseLink: String
) {

    Row {
        Button(
            modifier = Modifier
                .padding(10.dp),
            colors = ButtonDefaults.buttonColors(backgroundColor = main_dark_blue),
            shape = RoundedCornerShape(4.dp),
            onClick = {
                navController.navigate("home")
            }
        ) {
            Text(
                text = "Home",
                color = white,
                modifier = Modifier.background(main_dark_blue)
            )
        }
        Button(
            modifier = Modifier
                .padding(10.dp),
            colors = ButtonDefaults.buttonColors(backgroundColor = main_dark_blue),
            shape = RoundedCornerShape(4.dp),
            onClick = {
                navController.navigate(exerciseLink)
            }
        ) {
            Text(
                text = "Übung",
                color = white,
                modifier = Modifier.background(main_dark_blue)
            )
        }
    }
}