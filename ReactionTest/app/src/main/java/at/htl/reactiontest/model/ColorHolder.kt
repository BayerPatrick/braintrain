package at.htl.reactiontest.model

import androidx.compose.ui.graphics.Color

// Model for to save name to colors
// used for check users success/faults in exercises
data class ColorHolder(
    val name: String,
    val color: Color
)