package at.htl.reactiontest

import android.content.Context
import androidx.compose.foundation.ExperimentalFoundationApi
import androidx.compose.ui.test.*
import androidx.compose.ui.test.junit4.createAndroidComposeRule
import androidx.navigation.testing.TestNavHostController
import androidx.room.Room
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.platform.app.InstrumentationRegistry
import at.htl.reactiontest.database.AppDatabase
import at.htl.reactiontest.database.ResultDAO
import at.htl.reactiontest.model.Result
import at.htl.reactiontest.view.exercises.BrainTrain01Screen
import at.htl.reactiontest.view.exercises.BrainTrain02Screen
import at.htl.reactiontest.view.exercises.BrainTrain03Screen
import at.htl.reactiontest.view.exercises.FooterResult
import at.htl.reactiontest.viewModel.exercisesList
import org.junit.After
import org.junit.Assert.assertEquals
import org.junit.Assert.assertTrue
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import java.io.IOException
import java.time.LocalDate
import kotlin.math.roundToInt

// für Tests wird ein Emulator/Phone benötigt

/**
 * Instrumented test, which will execute on an Android device.
 *
 * See [testing documentation](http://d.android.com/tools/testing).
 */
@RunWith(AndroidJUnit4::class)
class BrainTrainTest {

    // StandardTest
    @Test
    fun useAppContext() {
        // Context of the app under test.
        val appContext = InstrumentationRegistry.getInstrumentation().targetContext
        assertEquals("at.htl.reactiontest", appContext.packageName)
    }


    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    private lateinit var navController: TestNavHostController

    /*
    private lateinit var brainTrainViewModel: BrainTrainViewModel
    private val viewModel = composeTestRule.activity.viewModels<BrainTrainViewModel>().value
    */

    @Before
    fun setUp() {
        navController = TestNavHostController(ApplicationProvider.getApplicationContext())
    }

    // checks if homeScreen is showed when you start the app
    @Test
    fun home_screen_test() {
        composeTestRule
            .onNodeWithText("Welcome to")
            .assertExists()
    }

    // checks if all exercises are shown on the home screen
    // (4 because emulator screen can't show more on home screen
    // so if u test it on an other device, this test might fail)
    @Test
    fun check_if_all_exercise_exist() {
        composeTestRule
            .onAllNodesWithContentDescription("Play")
            .assertCountEquals(4)
    }

    // checks if all tests are leading to an different screen
    @Test
    fun check_if_all_exercise_views_exist() {
        composeTestRule
            .onAllNodesWithContentDescription("Play")
            .assertAll(hasClickAction())
    }

    // checks if the navigation from the homeScreen to the exercisesScreen (firstOne) works
    @Test
    fun home_to_exercise_navigation_test() {
        composeTestRule
            .onAllNodes(hasContentDescription("Play"))[0]
            .performClick()

        composeTestRule
            .onNodeWithText("Versuche so schnell wie möglich, auf das Rechteck mit der richtigen Farbe zu drücken!")
            .assertExists()
    }

    // tests if exercise 1 works
    @ExperimentalFoundationApi
    @Test
    fun brainTrain_01_test() {

        composeTestRule.setContent {
            BrainTrain01Screen(navController = navController, brainTrain = exercisesList[0])
        }

        // LazyVerticalGrid ist nicht wirklich möglich momentan zu testen
        composeTestRule
            .onNodeWithText("Start")
            .assertHasClickAction()

    }

    // tests if exercise 2 works
    @ExperimentalFoundationApi
    @Test
    fun brainTrain_02_test() {
        composeTestRule.setContent {
            BrainTrain02Screen(navController = navController, brainTrain = exercisesList[1])
        }

        composeTestRule
            .onNodeWithText("Start").performClick()

        composeTestRule
            .onNodeWithText("WAIT!").performClick()

        // test if error function works
        composeTestRule
            .onNodeWithText("Too Early!").assertExists()
    }

    // tests if exercise 3 works
    @ExperimentalFoundationApi
    @Test
    fun brainTrain_03_test() {

        composeTestRule.setContent {
            BrainTrain03Screen(navController = navController, brainTrain = exercisesList[2])
        }

        // LazyVerticalGrid ist nicht wirklich möglich momentan zu testen
        composeTestRule
            .onNodeWithText("Start")
            .assertHasClickAction()
    }
}

@RunWith(AndroidJUnit4::class)
class ResultTest {

    private lateinit var brainTrainDao: ResultDAO
    private lateinit var db: AppDatabase

    @get:Rule
    val composeTestRule = createAndroidComposeRule<MainActivity>()

    // create database for tests
    @Before
    fun createDb() {
        val context = ApplicationProvider.getApplicationContext<Context>()
        db = Room.inMemoryDatabaseBuilder(
            context, AppDatabase::class.java
        ).build()
        brainTrainDao = db.brainTrainDAO()
    }

    // close database after test
    @After
    @Throws(IOException::class)
    fun closeDb() {
        db.close()
    }

    // test if add result method works
    @Test
    fun add_result_test() {

        // add exampleResult
        brainTrainDao.addResult(
            Result(
                brainTrainName = "result_test",
                createdAt = LocalDate.now(),
                resultMills = 100
            )
        )

        composeTestRule.setContent {
            FooterResult(brainTrainName = "result_test")
        }

        val resultsList = brainTrainDao.getAllResults()

        // check if insert result is in database
        assertTrue(resultsList[0].resultMills == 100L)
    }

    @Test
    fun average_result_test() {

        // add test results
        //region addResults
        brainTrainDao.addResult(
            Result(
                brainTrainName = "result_test",
                createdAt = LocalDate.now(),
                resultMills = 100
            )
        )

        brainTrainDao.addResult(
            Result(
                brainTrainName = "result_test",
                createdAt = LocalDate.now(),
                resultMills = 200
            )
        )
        //endregion

        // code out of function (can't use function)
        //region funCode
        var sum = 0L
        var counter = 0
        var highScore: Long = 0

        val originalList = brainTrainDao.getAllResults()

        for (l in originalList) {
            if (l.resultMills <= highScore) {
                highScore = l.resultMills
            } else if (highScore == 0L) {
                highScore = l.resultMills
            }
            sum += l.resultMills
            counter += 1
        }

        val calc = sum.toFloat() / originalList.size.toFloat()

        val avg = calc.roundToInt()
        //endregion

        // check average score
        assertTrue(avg == 150)

        // check highest score
        assertTrue(highScore == 100L)
    }

}


